@RestResource(urlMapping='/upload/volunteer')
global with sharing class Rest_ServicesGetUpload {
    global class customException extends Exception {}

    global class PostRequestRecord{
        global String idNumber;
        global String lastName;
        global String firstName;
        global String adress;
        global String email;
        global String phone;
       
        global PostRequestRecord(String idNumber, String lastName, String firstName, String adress, String email, String phone){
            this.idNumber = idNumber;
            this.lastName = lastName;
            this.firstName = firstName;
            this.adress = adress;
            this.email = email;
            this.phone = phone;
        }
    }

    global class PostRequestWrapper{
        global String recordtype;
        global List<PostRequestRecord> records;

        global PostRequestWrapper(String recordtype, List<PostRequestRecord> records){
            this.recordtype = recordtype;
            this.records = records;
        }
    }

    global class ResponseWrapper{
        global String errorMsg { get; set; }
        global String status {get; set; }
        global Integer statusCode {get; set; }
       
        global ResponseWrapper(String msg, String status, Integer statusCode){
            this.errorMsg = msg;
            this.status = status;
            this.statusCode =  statusCode;
        }
    }
    
    @HttpPost
    global static ResponseWrapper doPost() {
        
        RestRequest theRequest = RestContext.request;
        PostRequestWrapper wrapper;
		ResponseWrapper response;
        String body;
        try {
            body = theRequest.requestBody.toString();
            System.debug('body: ' + body);
            wrapper = (PostRequestWrapper)JSON.deserialize(body, PostRequestWrapper.class);
            if(wrapper == null)
            {
                throw new customException('Error: NO data.');
            }

            response = new ResponseWrapper('', 'OK', 200);
            // RestContext.response.addHeader('Content-Type', 'application/json');
            // RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));

        } catch (Exception ex) {
            System.debug('ex: ' + ex);
            String errMsg = ex.getMessage();
            response = new ResponseWrapper(errMsg, 'ERROR', 500);
            // RestContext.response.addHeader('Content-Type', 'application/json');
            // RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        }

        return response;
    }

}

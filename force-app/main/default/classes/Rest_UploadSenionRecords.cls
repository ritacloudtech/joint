@RestResource(urlMapping='/retrive/records')
global with sharing class Rest_UploadSenionRecords {
    global class customException extends Exception {}

    global class PostRequestWrapper{
        global String lasttimestamp;
        
        global PostRequestWrapper(String lasttimestamp){
            this.lasttimestamp = lasttimestamp;
        }
    }

    global class ResponseWrapper{
        global String status {get; set; }
        global String data {get; set; }
        global Integer statusCode {get; set; }
       
        global ResponseWrapper(String status, Integer statusCode, String data){
            this.status = status;
            this.statusCode =  statusCode;
            this.data = data;
        }
    }

    @HttpPost
    global static ResponseWrapper doPost() {
        
        RestRequest theRequest = RestContext.request;
        PostRequestWrapper wrapper;
		ResponseWrapper response;
        String body;
        try {
            body = theRequest.requestBody.toString();
            System.debug('body: ' + body);
            wrapper = (PostRequestWrapper)JSON.deserialize(body, PostRequestWrapper.class);
            if(wrapper == null)
            {
                throw new customException('Error: NO data.');
            }
            List<Contact> contacts = [SELECT Id, LastName, FirstName, Name, MobilePhone, LastModifiedDate, Email, Title, Description, City__c, Street__c, House_number__c, Entrance__c FROM Contact LIMIT 100];
            String jsonContacts = JSON.serialize(contacts);
            response = new ResponseWrapper('Success', 200, jsonContacts);
        } catch (Exception ex) {
            System.debug('ex: ' + ex);
            String errMsg = ex.getMessage();
            response = new ResponseWrapper('ERROR', 500, errMsg);
            // RestContext.response.addHeader('Content-Type', 'application/json');
            // RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        }

        return response;
    }
}
